import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private authLien="http://127.0.0.1:3003/users/login/"
  private signupLien="http://127.0.0.1:3003/users/signup/"
  private getUserLien="http://127.0.0.1:3003/users/"

  private user_info = null
  private unableToLoadUserInfoText = 'An error occured while loading user information. Unable to retrieve the current user\'s information'
  private httpOptions = {headers:new HttpHeaders({
    'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem("userOnline")).token
  })}

  constructor(private http: HttpClient, private alertController : AlertController) { }

  login(data:{}): Observable<any>{
    return this.http.post(this.authLien, data,this.httpOptions);
  }

  signup(data:{}): Observable<any>{
    return this.http.post(this.signupLien, data, this.httpOptions);
  }

  public loadUserInfo(){
    // await localStorage.get('user_info').then(
    //   res=>{
    //     this.setUserInfo(res)
    //   },
    //   err=>{
    //     this.UserInfoError(this.unableToLoadUserInfoText)
    //   })
    this.setUserInfo(JSON.parse(localStorage.getItem('userOnline')!)) 
    return this.getUserInfo()
  }

  public getUserInfo() {
    return this.user_info
    // return this.user_info
  }

  public setUserInfo(user_info){
    this.user_info = user_info
  }

  async UserInfoError(text) { 
    const alert = await this.alertController.create(
    { header: 'System Error', 
      subHeader: 'An error occured in the app', 
      message: text, 
      buttons: ['OK'] 
    }); 
    await alert.present(); 
    const { role } = await alert.onDidDismiss(); 
  }

  getUser(id:String):Observable<any>{
    return this.http.get(this.getUserLien+id, this.httpOptions);
  }

  updateUser(id:String,data:{}):Observable<any>{
    return this.http.put(this.getUserLien+id,data, this.httpOptions);
  }

  deleteUser(id:String,):Observable<any>{
    return this.http.delete(this.getUserLien+id,this.httpOptions);
  }
}
