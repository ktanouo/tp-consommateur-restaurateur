import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  private getDishLien="http://127.0.0.1:3003/dishes/"
  private getMenuLien="http://127.0.0.1:3003/menu/"
  private httpOptions = {headers:new HttpHeaders({
    'Authorization' : 'Bearer ' + JSON.parse(localStorage.getItem("userOnline")).token
  }),}

  constructor(private http:HttpClient) { }

  getAllDishes():Observable<any>{
    return this.http.get(this.getDishLien, this.httpOptions)
  }

  getDish(id:String):Observable<any>{
    return this.http.get(this.getDishLien+id, this.httpOptions)
  }

  deleteDish(id:String):Observable<any>{
    return this.http.delete(this.getDishLien+id, this.httpOptions)
  }

  updateDish(id:String,data:{}):Observable<any>{
    return this.http.put(this.getDishLien+id,data, this.httpOptions)
  }

  addDish(data:{}):Observable<any>{
    return this.http.post(this.getDishLien, data, this.httpOptions)
  }

  // For Menus
  getAllMenus():Observable<any>{
    return this.http.get(this.getMenuLien, this.httpOptions )
  }

  addMenu(data:{}):Observable<any>{
    return this.http.post(this.getMenuLien, data ,this.httpOptions )
  }

  deleteMenu(id:String):Observable<any>{
    return this.http.delete(this.getMenuLien+id, this.httpOptions )
  }

  updateMenu(id:String,data:{}):Observable<any>{
    return this.http.put(this.getMenuLien+id, data, this.httpOptions)
  }

  deleteSeveralMenus(ids:any[]):Observable<any>{
    this.httpOptions['body']={'ids':ids}
    return this.http.delete(this.getMenuLien, this.httpOptions)
  }
}
