import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  // },
  {
    path: 'signup',
    loadChildren: () => import('./authentification/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./authentification/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'consommateur/homepage',
    loadChildren: () => import('./consommateur/homepage/homepage.module').then( m => m.HomepagePageModule)
  },
  {
    path: 'consommateur/update-info',
    loadChildren: () => import('./consommateur/update-info/update-info.module').then( m => m.UpdateInfoPageModule)
  },
  {
    path: 'restaurateur/dish-form',
    loadChildren: () => import('./restaurateur/dish-form/dish-form.module').then( m => m.DishFormPageModule)
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'restaurateur/homepage',
    loadChildren: () => import('./restaurateur/homepage/homepage.module').then( m => m.HomepagePageModule)
  },
  {
    path: 'restaurateur/menus',
    loadChildren: () => import('./restaurateur/menus/menus.module').then( m => m.MenusPageModule)
  },
  {
    path: 'restaurateur/menu-form',
    loadChildren: () => import('./restaurateur/menu-form/menu-form.module').then( m => m.MenuFormPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
