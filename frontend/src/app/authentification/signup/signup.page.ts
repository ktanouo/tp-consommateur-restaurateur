import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  signupForm : FormGroup;
  isSubmitted = false;
  userOnline:any;

  constructor(
    private formBuilder : FormBuilder,
    private alertController:AlertController,
    public loadingController: LoadingController,
    private userServ: UserService,
    private navController:NavController
  ) { }

  ngOnInit() {
    this.signupForm= this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      firstName: ['', Validators.compose([Validators.required])],
      lastName: ['', Validators.compose([Validators.required])],
      dateOfBirth: ['', Validators.compose([Validators.required])],
      role: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      
    });
  }

  passwordType: string ='password';
  passwordIcon: string ='eye-off';
  hideShowPassword(){
   this.passwordType = this.passwordType === 'text' ? 'password': 'text';
   this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye': 'eye-off';
  }

  get errorControl(){
    return this.signupForm.controls;
  }

  async echecAuthentification() {
    const alert = await this.alertController.create({
      header: 'Echec d authentification',
      message: 'Ces identifiants sont incorrects',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async echecConnexion() {
    const alert = await this.alertController.create({
      header: 'Echec de connexion',
      message: 'Probleme de Connexion à la base de données',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Veuillez patienter...',
      duration: 2000
    });
    //localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

  }

  onSubmit(){
    this.isSubmitted=true;
    console.log(this.signupForm.value)
    this.userServ.signup(this.signupForm.value).subscribe(
      res=>{

        console.log(res);
        if (res===null) {
          this.presentLoading().then(()=>{
            this.echecAuthentification();
          })
        } else {
          this.userOnline=res;
          localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
          this.presentLoading()
          .then(()=>{
          //this.router.navigate(["/onglet/homepage"]);
          this.navController.navigateRoot(["/login"])
        })
        } 
      },
      err=>{ 
        this.presentLoading().then(()=>{
          this.echecConnexion();
        })      
        console.log("erreur lors de la connexion")
        console.log(err)
      }

    )    
  }

}
