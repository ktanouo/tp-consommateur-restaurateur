import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm : FormGroup;
  isSubmitted = false;
  userOnline:any;

  constructor(private formBuilder : FormBuilder,
    private alertController:AlertController,
    public loadingController: LoadingController,
    private userServ: UserService,
    private navController:NavController
  ) { }

  ngOnInit() {
    this.loginForm= this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      
    });
  }

  passwordType: string ='password';
  passwordIcon: string ='eye-off';
  hideShowPassword(){
   this.passwordType = this.passwordType === 'text' ? 'password': 'text';
   this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye': 'eye-off';
  }

  get errorControl(){
    return this.loginForm.controls;
  }

  async echecAuthentification() {
    const alert = await this.alertController.create({
      header: 'Echec d authentification',
      message: 'Ces identifiants sont incorrects',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async echecConnexion() {
    const alert = await this.alertController.create({
      header: 'Echec de connexion',
      message: 'Probleme de Connexion à la base de données',
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Veuillez patienter...',
      duration: 2000
    });
    //localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

  }

  onSubmit(){
    this.isSubmitted=true;
    let data={
      password: this.loginForm.get('password').value,
      email: this.loginForm.get('email').value,
    }
    this.userServ.login(data).subscribe(
      res=>{

        console.log(res);
        if (res===null) {
          this.presentLoading().then(()=>{
            this.echecAuthentification();
          })
        } else {
          this.userOnline=res;
          localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
          this.presentLoading()
          .then(()=>{
            if (this.userOnline.userRole === "RESTAURATEUR") {
              this.navController.navigateRoot(["/restaurateur/homepage"])
            } else {
              this.navController.navigateRoot(["/consommateur/homepage"])
            }
          //this.router.navigate(["/onglet/homepage"]);
          // this.navController.navigateRoot(["/onglet/homepage"])
        })
        } 
      },
      err=>{ 
        this.presentLoading().then(()=>{
          this.echecConnexion();
        })      
        console.log("erreur lors de la connexion")
        console.log(err)
      }

    )    
  }

}
