import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss'],
})
export class HomepagePage implements OnInit {

  user_info=null
  date_of_today = Date().slice(0,15)
  constructor(private userService: UserService, private navController: NavController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.user_info =this.userService.loadUserInfo()
  }

  ionViewWillLeave(){
  }

  logout(){
    this.navController.navigateRoot(["/login"])
    localStorage.removeItem('userOnline')
  }

  deleteAccount(){
    this.userService.deleteUser(this.user_info.userId).subscribe((data) => {
      console.log('User Deleted successfully');
      this.navController.navigateRoot(["/login"])
    })
  }

}
