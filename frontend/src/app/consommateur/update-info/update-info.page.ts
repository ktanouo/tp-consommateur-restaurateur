import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-info',
  templateUrl: './update-info.page.html',
  styleUrls: ['./update-info.page.scss'],
})
export class UpdateInfoPage implements OnInit {

  userForm : FormGroup;
  isSubmitted = false;
  user_info = null

  constructor(
    private formBuilder : FormBuilder,
    private alertController:AlertController,
    public loadingController: LoadingController,
    private userService: UserService,
    private navController:NavController
  ) { }

  passwordType: string ='password';
  passwordIcon: string ='eye-off';
  hideShowPassword(){
    this.passwordType = this.passwordType === 'text' ? 'password': 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye': 'eye-off';
   }

  ngOnInit() {
    this.user_info =this.userService.loadUserInfo()
    console.log(this.user_info);
    this.userForm= this.formBuilder.group({
    firstName: [this.user_info.userFirstName, Validators.compose([Validators.required])],
    lastName: [this.user_info.userLastName, Validators.compose([Validators.required])],
    dateOfBirth: [null, Validators.compose([Validators.required])],
    });
    this.userService.getUser(this.user_info.userId).subscribe((user) =>
      {
        var curr_date: String =
        new Date(user.dateOfBirth).getFullYear() + "-";
        if (new Date(user.dateOfBirth).getMonth() >= 10) {
          curr_date +=
            String(new Date(user.dateOfBirth).getMonth() + 1) + "-";
        } else {
          curr_date +=
            String("0" + (new Date(user.dateOfBirth).getMonth() + 1)) + "-";
        }
        if (new Date(user.dateOfBirth).getDate() >= 10) {
          curr_date += String(new Date(user.dateOfBirth).getDate());
        } else {
          curr_date += String("0" + new Date(user.dateOfBirth).getDate());
        }
        this.userForm.value.dateOfBirth = curr_date
      }
    )
  }

  ionViewDidEnter(){
    this.ngOnInit()
  }

  onSubmit(){
    console.log(this.user_info.userId,this.userForm.value);
    this.userService.updateUser(this.user_info.userId,this.userForm.value).subscribe((result)=>{
      this.navController.navigateRoot(["/consommateur/homepage"])
    })
  }



}
