import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DishService } from 'src/app/services/dish.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.page.html',
  styleUrls: ['./menus.page.scss'],
})
export class MenusPage implements OnInit {

  listMenus:any
  user_info: any = null
  constructor(private dishService: DishService, private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    // this.ngOnInit()
    this.getMenus()
    this.user_info = this.userService.loadUserInfo()
  }

  getMenus(){
    this.dishService.getAllMenus().subscribe((menus)=>{
      this.listMenus = menus.menus
      // console.log(object);
      this.listMenus = this.listMenus.filter((menu:any)=>{
        return menu.restaurateurId == this.user_info.userId
      })
    })
  }

  deleteMenu(id:String){
    this.dishService.deleteMenu(id).subscribe((ans)=>
      {
        // this.router.navigate(["/restaurateur/homepage"]);
        window.location.reload();
    }
    )
  }

  editMenu(id:String){
    var menu = this.listMenus.find((dish:any) => {
      return dish._id == id
    })
    localStorage.setItem('menu', JSON.stringify(menu))
    this.router.navigate(["/restaurateur/menu-form"])
  }

}
