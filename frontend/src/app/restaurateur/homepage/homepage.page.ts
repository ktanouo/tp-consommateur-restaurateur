import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { DishService } from '../../services/dish.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss'],
})
export class HomepagePage implements OnInit {

  listDishes : any
  user_info: any = null
  constructor(
    private userService:UserService, 
    private dishService: DishService,
    private navController: NavController,
    private loadingController: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    // this.ngOnInit()
    this.getDishes()
    this.user_info = this.userService.loadUserInfo()
  }

  getDishes(){
    this.dishService.getAllDishes().subscribe((dishes) => {
      this.listDishes = dishes.plats
      this.listDishes = this.listDishes.filter((dish:any)=>{
        return dish.restaurateurId == this.user_info.userId
      })
    })
  }

  editDish(id:String){
    var plat = this.listDishes.find((dish:any) => {
      return dish._id == id
    })
    localStorage.setItem('plat', JSON.stringify(plat))
    this.router.navigate(["/restaurateur/dish-form"])
  }

  deleteDish(id:String, name:String){
    
    this.dishService.deleteDish(id).subscribe((resp)=>{
      this.dishService.getAllMenus().subscribe((res)=>{
        res.menus.forEach(menu => {
          const index = menu.plats.indexOf(name);
          if (index > -1) {
              menu.plats.splice(index,1)
          }
          this.dishService.updateMenu(menu._id,menu).subscribe((result)=>{
            if(menu.plats.length<=1){
              this.dishService.deleteMenu(menu._id).subscribe((res)=>{
                // console.log('deleted');
              })
              }
          })
        })
        console.log('All Menus Deleted');
        this.presentLoading().then(()=>{
          // this.router.navigate(['/restaurateur/menus'])
          window.location.reload()
        })
      })
    })
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Veuillez patienter...',
      duration: 1000
    });
    //localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

  }

  logout(){
    this.navController.navigateRoot(["/login"])
    localStorage.removeItem('userOnline')
  }

  deleteAccount(){
    this.userService.deleteUser(this.user_info.userId).subscribe((data) => {
      console.log('Dish Deleted successfully');
      this.navController.navigateRoot(["/login"])
    })
  }

}
