import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { DishService } from 'src/app/services/dish.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dish-form',
  templateUrl: './dish-form.page.html',
  styleUrls: ['./dish-form.page.scss'],
})
export class DishFormPage implements OnInit {

  dishForm : FormGroup;
  isSubmitted = false;
  dishToUpdate = null;
  buttonText = "Create Dish"
  categories = ['ENTREE', 'RESISTANCE', 'DESSERT']
  constructor(
    private formBuilder : FormBuilder,
    private alertController:AlertController,
    public loadingController: LoadingController,
    private dishServ: DishService,
    private userService: UserService ,
    private router : Router
  ) { }

  get errorControl(){
    return this.dishForm.controls;
  }

  ngOnInit() {
    this.dishToUpdate = JSON.parse(localStorage.getItem('plat')!)
    if(this.dishToUpdate != null){
      this.buttonText = "Update Dish"
      this.dishForm= this.formBuilder.group({
        name: [this.dishToUpdate.name, Validators.compose([Validators.required])],
        description: [this.dishToUpdate.description, Validators.compose([Validators.required, Validators.minLength(4)])],
        prix: [this.dishToUpdate.prix, Validators.compose([Validators.required])],
        categorie: [this.dishToUpdate.categorie, Validators.compose([Validators.required])],
      });
    }
    else{
      this.dishForm= this.formBuilder.group({
        name: ['', Validators.compose([Validators.required])],
        description: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
        prix: ['', Validators.compose([Validators.required])],
        categorie: ['', Validators.compose([Validators.required])],
      });
    }
  }

  ionViewWillEnter(){
    this.ngOnInit()
  }

  ionViewDidLeave(){
    localStorage.removeItem('plat')
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Veuillez patienter...',
      duration: 1000
    });
    //localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

  }

  onSubmit(){
    var data = this.dishForm.value
    data.restaurateurId = this.userService.loadUserInfo().userId
    console.log(data);
    if (this.dishToUpdate != null) {
      this.dishServ.updateDish(this.dishToUpdate._id,data).subscribe((result)=>{
        this.presentLoading().then(()=>{
          this.router.navigate(["/restaurateur/homepage"])
        })
      })
    } else {
      this.dishServ.addDish(data).subscribe((result)=>{
        this.presentLoading().then(()=>{
          this.router.navigate(["/restaurateur/homepage"])
        })
      })
    }
  }

}
