import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DishFormPageRoutingModule } from './dish-form-routing.module';

import { DishFormPage } from './dish-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DishFormPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [DishFormPage]
})
export class DishFormPageModule {}
