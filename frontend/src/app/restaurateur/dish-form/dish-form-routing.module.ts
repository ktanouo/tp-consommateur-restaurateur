import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DishFormPage } from './dish-form.page';

const routes: Routes = [
  {
    path: '',
    component: DishFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DishFormPageRoutingModule {}
