import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuFormPageRoutingModule } from './menu-form-routing.module';

import { MenuFormPage } from './menu-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuFormPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [MenuFormPage]
})
export class MenuFormPageModule {}
