import { Component, OnInit } from '@angular/core';
import { DishService } from 'src/app/services/dish.service';
import { UserService } from 'src/app/services/user.service';
import { FormArray, FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-menu-form',
  templateUrl: './menu-form.page.html',
  styleUrls: ['./menu-form.page.scss'],
})
export class MenuFormPage implements OnInit {

  listDishes : any
  user_info: any = null
  buttonText = "Create"
  menuForm:any
  menuToUpdate:any = null
  constructor(
    private dishService: DishService, 
    private userService: UserService, 
    private formBuilder : FormBuilder,
    public loadingController: LoadingController,
    private router: Router
    ) { }

  ngOnInit() {
    this.menuToUpdate = JSON.parse(localStorage.getItem('menu'))
    if (this.menuToUpdate != null) {
      this.buttonText = "Update"
      this.menuForm= this.formBuilder.group({
        name:[this.menuToUpdate.name, Validators.compose([Validators.required])],
        description: [this.menuToUpdate.description, Validators.compose([Validators.required, Validators.minLength(5)])],
        plats: this.getSelectedDishes(this.menuToUpdate),
      });
    } else {
      this.menuForm= this.formBuilder.group({
        name:['', Validators.compose([Validators.required])],
        description: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
        plats: new FormArray([],[Validators.compose([Validators.required,Validators.minLength(2)])]),
      });
    }
    
  }
  

  ionViewWillEnter(){
    this.getDishes()
    this.user_info = this.userService.loadUserInfo()
    this.ngOnInit()
  }

  ionViewDidLeave(){
    localStorage.removeItem('menu')
  }

  getDishes(){
    this.dishService.getAllDishes().subscribe((dishes) => {
      this.listDishes = dishes.plats
      this.listDishes = this.listDishes.filter((dish:any)=>{
        return dish.restaurateurId == this.user_info.userId
      })
    })
  }

  onCheckboxChange(event: any) {
    // Cette fonction permet de recuperer les cases cochées avant qu'on ne soumette le formulaire
    const selectedDishes = (this.menuForm.controls.plats as FormArray);
    if (event.target.checked) {
      selectedDishes.push(new FormControl(event.target.value));
    } else {
      const index = selectedDishes.controls
      .findIndex(x => x.value === event.target.value);
      selectedDishes.removeAt(index);
    }
  }

  ischecked(nameDish: any){
    if(this.menuToUpdate){
      let array :[any] = this.menuToUpdate.plats;
      let found = array.find((dish)=>{
        return nameDish == dish
      })
      return found != undefined
    }else{
      return false;
    }
    
  }

  getSelectedDishes(menuToUpdate){
    const selectedDishesName = new FormArray([]);
    menuToUpdate.plats.forEach((plat:any)=>{
      selectedDishesName.push(new FormControl(plat))
    });
    return selectedDishesName;
  }

  // returnDishes(dishnames:[]){
  //   this.listDishes.forEach(element => {
  //     dishnames.find((dishname)=>{
  //       return dishname==element.name
  //     })
  //   });
  // }

  onSubmit(){
    
    var data = this.menuForm.value
    data.restaurateurId = this.userService.loadUserInfo().userId
    if (this.menuToUpdate != null) {
      console.log( data);
      this.dishService.updateMenu(this.menuToUpdate._id,data).subscribe((result)=>{
        // console.log(data, this.menuToUpdate != null);
        this.presentLoading().then(()=>{
          console.log('Menu Updated Succesfully');
          this.router.navigate(["/restaurateur/menus"])
        })
      })
    } else {
      this.dishService.addMenu(data).subscribe((response) =>
      {
        console.log('Menu Created Succesfully');
        this.router.navigate(['/restaurateur/menus'])
      }
    )
    }
    
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Veuillez patienter...',
      duration: 1000
    });
    //localStorage.setItem("userOnline",JSON.stringify(this.userOnline));
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

  }

}
