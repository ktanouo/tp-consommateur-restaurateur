const mongoose = require("mongoose");
const uniqueValidator = require('mongoose-unique-validator');

const UserSchema = mongoose.Schema({
    firstName: { type: String, required: true, trim: true },
    lastName: { type: String, required: true, trim: true },
    role: { type: String, enum: ['CONSOMMATEUR','RESTAURATEUR'], default: 'CONSOMMATEUR', required: true, trim: true },
    dateOfBirth: { type: Date, require: true },

    // For auth
    email: { type: String, required: true, trim: true , unique: true},
    password: { type: String, required: true,},
});

UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User',UserSchema);